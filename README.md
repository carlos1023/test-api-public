# WebServer + RestServer
0. Cambiar el nombre del archivo ```.example.env``` a ```.env```
1. Ejecutar ```npm install``` para construir los módulos de Node.
2. Ejecutar  ```nodemon app``` que ejecuta el proyecto en el pueto 8080
3. Tener en cuenta que para crear Usuarios se deben crear con alguno de estos dos roles:
    ADMIN_ROLE
	USER_ROLE
4. Tener en cuenta que para borrar un usuario se debe hacer el login con un usuario creado,
    copiar el token en la API de borrado se debe pasar por parametro en el los header con el nombre ```x-token``` 
5. Se adiciona la colección de postman para pruebas.
6. Las funcionalidades de usuario y login se realiza por medio de MongoDB  y la de consumo de API trae el listado por ID
    de la API pasada en el documento.
