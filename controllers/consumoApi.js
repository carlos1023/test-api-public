const { response } = require('express');
const axios = require('axios');

const getApi = async(req, res = response) => {
    try {
        // Realizar la solicitud GET
        const resp = await axios.get(`https://jsonplaceholder.typicode.com/todos/${req.query.id}`);

        // Enviar los datos de la respuesta
        res.json(resp.data);
    } catch (error) {
        console.error('error1', error); // Mejor manejo de logs de errores
        // Enviar un mensaje de error más general
        res.status(500).json({
            msg: 'Error al obtener los datos de la API externa'
        });
    }
}

module.exports = {
    getApi
};
